#!/usr/bin/env python2.7

import unittest
from dnaseqlib import * 

### Utility classes ###

# Maps integer keys to a set of arbitrary values.
class Multidict:
    # Initializes a new multi-value dictionary, and adds any key-value
    # 2-tuples in the iterable sequence pairs to the data structure.
    def __init__(self, pairs=[]):
        self.mdict ={}
        #raise Exception("Not implemented!")
    # Associates the value v with the key k.
    def put(self, k, v):
        if k in self.mdict:
            self.mdict[k].append(v)
        else:
            self.mdict[k] =[v]
        #raise Exception("Not implemented!")
    # Gets any values that have been associated with the key k; or, if
    # none have been, returns an empty sequence.
    def get(self, k):
        if k in self.mdict:
            return self.mdict[k]
        else:
            return []
        #raise Exception("Not implemented!")

# Given a sequence of nucleotides, return all k-length subsequences
# and their hashes.  (What else do you need to know about each
# subsequence?)
def subsequenceHashes(seq, k):
    try:
        subseq = ''
        while len(subseq) < k:
            subseq += seq.next()
        hashy = RollingHash(subseq)
        nexty = ""
        while True:
            while len(subseq) < k:
                nexty = seq.next()
                subseq += nexty
            if nexty =="":
                yield (subseq,hashy.current_hash())
            else:
                hashy.slide(subseq[0],nexty)
                yield (subseq,hashy.current_hash())
            subseq = subseq[1:]
    except StopIteration:
        return
    #raise Exception("Not implemented!")


# Similar to subsequenceHashes(), but returns one k-length subsequence
# every m nucleotides.  (This will be useful when you try to use two
# whole data files.)
def intervalSubsequenceHashes(seq, k, m):
    try:
        subseq = ''
        while len(subseq) < k:
            #subseq += seq.next()
            subseq +=next(seq)
        hashy = RollingHash(subseq)
        nexty = ""
        while True:
            while len(subseq) < k:
                #nexty = seq.next()
                nexty = next(seq)
                subseq += nexty
            if nexty =="":
                yield (subseq,hashy.current_hash())
            else:
                hashy.slide(subseq[0],nexty)
                yield (subseq,hashy.current_hash())
            if m ==k:
                subseq = ""
            elif m < k:
                subseq=subseq[m:]
            else:
                for i in range(0,m-k):
                    temp = next(seq)
                    #temp = seq.next()
                subseq =""
    except StopIteration:
        return
    raise Exception("Not implemented!")


# Searches for commonalities between sequences a and b by comparing
# subsequences of length k.  The sequences a and b should be iterators
# that return nucleotides.  The table is built by computing one hash
# every m nucleotides (for m >= k).
def getExactSubmatches(a, b, k, m):
    A = intervalSubsequenceHashes(a,k,m) # generator object
    count_A =0
    for i in A:
        count_B =0
        B = intervalSubsequenceHashes(b,k,m)
        print(i)
        for j in B: # BE CAREFUL GENERATOR OBJECTS GET USED UP IN ONE LOOP
            print(i,j)
            if i[1] == j[1]:
                yield (count_A, count_B)
            count_B+=1
        count_A+=1
    #raise Exception("Not implemented!")
def alpha():
    for i in "ABCDEFGHIJKLMN":
        yield i
aseq = alpha()
bseq = alpha()
#b = intervalSubsequenceHashes(aseq,3,4)
c =getExactSubmatches(aseq,bseq,3,4)
for j in c:
    print(j)
print(c)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: {0} [file_a.fa] [file_b.fa] [output.png]'.format(sys.argv[0]))
        sys.exit(1)

    # The arguments are, in order: 1) Your getExactSubmatches
    # function, 2) the filename to which the image should be written,
    # 3) a tuple giving the width and height of the image, 4) the
    # filename of sequence A, 5) the filename of sequence B, 6) k, the
    # subsequence size, and 7) m, the sampling interval for sequence
    # A.
    compareSequences(getExactSubmatches, sys.argv[3], (500,500), sys.argv[1], sys.argv[2], 8, 100)
